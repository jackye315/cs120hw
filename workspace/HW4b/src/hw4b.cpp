/*
Jack Ye
600.120
4/3/16
HW4
(917) 530-6467
jye24
jye24@jhu.edu
 */

#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <vector>
#include <cstdlib>
#include "hw4b.hpp"

using namespace std;

void print_sentence(int command, vector<string> phrasebank){
	while(command != 0){
		for(unsigned int i=0; i<phrasebank.size();i++){
			if(phrasebank.at(i).find("<START>") != string::npos){
				cout<<phrasebank.at(i)<< " ";
			}
		}
		int randomnum=rand() % phrasebank.size();
		while(phrasebank.at(randomnum).find("<END>") == string::npos){
			if(phrasebank.at(randomnum).find("<START>") != string::npos){

			}
			else{
				cout<<phrasebank.at(randomnum)<< " ";
			}
			randomnum=rand() % phrasebank.size();

		}
		cout<<phrasebank.at(randomnum) << endl;
		command--;
	}
}

int main(int argc, char *argv[]){
	
 
	if (argc < 2) {
		cout << "Need to enter command and filename!" << endl;
		return 1;  // exit program
	}

	int command = stoi(argv[1]);
	if(command < 0){
		cout << "Need to enter correct command!" << endl;
		return 1;
	}
	srand(1);
	string data1;
	string data2;
	int data3;
	vector<string> phrasebank;
	
	//create vector to hold all phrases
	while(cin >> data1){
	  cin >> data2;
	  data1=data1+ " " + data2;
	  cin >> data3;
	  while(data3 != 0){
	    phrasebank.push_back(data1);
	    data3=data3-1;
	  }
	}


	//generate and print the random sentences
	print_sentence(command, phrasebank);

	return 1;

}


/*
	while(command != 0){
		for(int i=0; i<phrasebank.size();i++){
			if(phrasebank.at(i).find("<START>") != string::npos){
				cout<<phrasebank.at(i)<< " ";
			}
		}
		int randomnum=rand() % phrasebank.size();
		while(phrasebank.at(randomnum).find("<END>") == string::npos){
			if(phrasebank.at(randomnum).find("<START>") != string::npos){

			}
			else{
			cout<<phrasebank.at(randomnum)<< " ";
			}
			randomnum=rand() % phrasebank.size();

		}
		cout<<phrasebank.at(randomnum) << endl;
		command--;
	}
*/
