/*
Jack Ye
600.120
2/2/16
HW1a
(917) 530-6467
jye24
jye24@jhu.edu
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[]){

	if (argc == 1) {
		printf("Need to enter a filename!");
		return 1;  // exit program
	}

	char file[40];
	strncpy(file, argv[1],40);
	FILE *fp=fopen(file, "r");
	FILE *fo=fopen("courseInts.txt", "w");
	char infile[255];


	//Main loop that each time, converts to binary and decimal
	while(fgets(infile, 255, (FILE*)fp) != 0){
	infile[15]='\0';
	char BinaryCode[33];


	//Department code as integer
	char Div[]="ASBUEDENMEPHPYSA";
	int x;
	int DivCode=0;
	for(x=0;x<16;x++){
		if(infile[0]==Div[x] && infile[1]==Div[x+1]){
			DivCode=x/2;
			x=x+strlen(Div);
		}
	}

	//Put Department into binary, place into array to store
	int a;
	for(a=0;a<3;a++){
		if ((DivCode >> (2-a)) & 1){
			BinaryCode[a]='1';
		}
		else{
			BinaryCode[a]='0';
		}
	}


	//Course Code as integer
	int courseCode;
	char coursecode[]={infile[3],infile[4],infile[5]};
	courseCode=atoi(coursecode);

	//Put Course code into binary, place into array
	int b;
	for(b=3;b<13;b++){
		if ((courseCode >> (12-b)) & 1){
			BinaryCode[b]='1';
		}
		else{
			BinaryCode[b]='0';
		}
	}


	//Course number as integer
	int courseNum;
	char coursenum[]={infile[7],infile[8],infile[9]};
	courseNum=atoi(coursenum);

	//Put course number into binary, store in array
	int c;
	for(c=13;c<23;c++){
		if ((courseNum >> (22-c)) & 1){
			BinaryCode[c]='1';
		}
		else{
			BinaryCode[c]='0';
		}
	}


	//Grades as integer
	char Grades[]="ABCDFISU";
	int i;
	int GradeLetter=0;
	for(i=0;i<8;i++){
		if(infile[10]==Grades[i]){
			GradeLetter=i;
			i=i+strlen(Grades);
		}
	}

	//Put grades into binary, store in array
	int d;
	for(d=23;d<26;d++){
		if ((GradeLetter >> (25-d)) & 1){
			BinaryCode[d]='1';
		}
		else{
			BinaryCode[d]='0';
		}
	}


	//Second part of grade as integer
	char Grade2[]="+-/";
	int y;
	int GradeSign=0;
	for(y=0;y<3;y++){
		if(infile[11]==Grade2[y]){
			GradeSign=y;
			y=y+strlen(Grade2);
		}
	}

	//Put second part into binary, store in array
	int e;
	for(e=26;e<28;e++){
		if ((GradeSign >> (27-e)) & 1){
			BinaryCode[e]='1';
		}
		else{
			BinaryCode[e]='0';
		}
	}


	//Credit as integer
	int credit=infile[12]-48;

	//Put credit into binary,store in array
	int f;
	for(f=28;f<31;f++){
		if ((credit >> (30-f)) & 1){
			BinaryCode[f]='1';
		}
		else{
			BinaryCode[f]='0';
		}
	}


	//Second part of credit, put in binary, store in array
	if(infile[14]=='5'){
		BinaryCode[31]='1';
	}else{
		BinaryCode[31]='0';
	}


	//Using binary, produce the decimal number
	int z;
	unsigned int DecimalCode=0;
	for(z=0;z<32;z++){
		if(BinaryCode[z]=='1'){
			int add=pow(2,(31-z));
			DecimalCode=DecimalCode + add;
		}
	}


	//Print stuff out and write into new file
	printf("%s  %u  %s\n", infile, DecimalCode, BinaryCode );
	fprintf(fo, "%u\n", DecimalCode);
	}
	fclose(fo);
	fclose(fp);

	return 0;
}
