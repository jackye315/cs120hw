/*
Jack Ye
600.120
2/2/16
HW3
(917) 530-6467
jye24
jye24@jhu.edu
 */
#ifndef HW3_H_
#define HW3_H_

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>

struct ccourse{
	char div[2];
	int department;
	int num;
	float assignment;
	char course[33];
};

struct tcourse{
	struct ccourse *basicinfo;
	char semester[7];
	char grade[3];
	struct tcourse *next;
};


void catinfo(struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[], FILE* output);
void catuptitle(struct ccourse catalog[], int counter, char tempdiv[], char tempdepartment[], char tempnum[], char newtitle[], FILE* output);
void catupcredit(struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[], char newcredit[],FILE* output);
char *split(char string[],int position, char ans[]);
void add(struct tcourse *curr, struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[], char tempsemester[],char tempgrade[],FILE* output);
struct tcourse *firstnode( struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[], char tempsemester[],char tempgrade[]);
bool compstrings(char sem1[], char sem2[]);
bool isabsent(struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[]);
void delete(struct tcourse *curr, char tempdiv[], char tempdepartment[], char tempnum[], char tempsemester[], FILE* output);
void getGPA(struct tcourse *curr, FILE* output);
void ptranscript(struct tcourse *head, FILE* output);
void pcatalog(struct ccourse *catalog, int counter, FILE* output);
bool tpresent(struct tcourse *curr, char tempdiv[], char tempdepartment[], char tempnum[]);
void makecatalog(FILE *fo, struct ccourse *catalog, char courseinfo[], int i, struct ccourse temp);
void tshowcourse(struct tcourse *curr, char tempdiv[], char tempdepartment[], char tempnum[], FILE* output);
bool tpresentadd(struct tcourse *curr, char tempdiv[], char tempdepartment[], char tempnum[], char tempsemester[]);
int fileeq(char lhsName[], char rhsName[]);






#endif /* HW3_H_ */
