/*
Jack Ye
600.120
2/2/16
HW3
(917) 530-6467
jye24
jye24@jhu.edu
 */

#include "HW3.h"

int main(){

	//test makecatalog and pcatalog
	FILE *fp=fopen("cat1.txt", "r");
	char infile [48];
	int counter =0;
	while(fgets(infile, 48, (FILE*)fp) != 0){
		//puts(infile);
		counter++;
	}
	//printf("%i\n",counter);
	fclose(fp);

	struct ccourse *catalog = malloc(counter * (int)sizeof(struct ccourse));
	char courseinfo[100];
	int i=0;
	struct ccourse temp;
	FILE *fo=fopen("cat1.txt", "r");

	makecatalog(fo, catalog, courseinfo, i, temp);



	//test catinfo
	char tempdiv[]= "EN";
	char tempdepartment[]="600";
	char tempnum[]="363";
	FILE *catinfo1= fopen("test1.txt", "w");
	catinfo(catalog, counter, tempdiv, tempdepartment, tempnum, catinfo1);
	fclose(catinfo1);

	FILE *catinfo2= fopen("test2.txt", "w");
	fprintf(catinfo2, "3.0 Introduction to Algorithms\n\n");
	fclose(catinfo2);
	assert(fileeq("test1.txt", "test2.txt"));



	//test catuptitle
	char newtitle[]="J";
	FILE *cattitle1= fopen("test1.txt", "w");
	catuptitle(catalog, counter, tempdiv, tempdepartment, tempnum, newtitle, cattitle1);
	catinfo(catalog, counter, tempdiv, tempdepartment, tempnum, cattitle1);
	fclose(cattitle1);

	FILE *cattitle2= fopen("test2.txt", "w");
	fprintf(cattitle2, "Updated course title3.0 J\n");
	fclose(cattitle2);
	assert(fileeq("test1.txt", "test2.txt"));


	char newtitle1[]="Jaasdsadadsadasd sads asd 12 3.5";
	FILE *cattitle3= fopen("test1.txt", "w");
	catuptitle(catalog, counter, tempdiv, tempdepartment, tempnum, newtitle1, cattitle1);
	catinfo(catalog, counter, tempdiv, tempdepartment, tempnum, cattitle3);
	fclose(cattitle3);

	FILE *cattitle4= fopen("test2.txt", "w");
	fprintf(cattitle2, "Updated course title3.0 Jaasdsadadsadasd sads asd 12 3.5\n");
	fclose(cattitle4);
	assert(fileeq("test1.txt", "test2.txt"));



	//test catupcredit
	char newcredit[]="3.5";


	FILE *catcred1= fopen("test1.txt", "w");
	catupcredit(catalog, counter, tempdiv, tempdepartment, tempnum, newcredit, catcred1);
	catinfo(catalog, counter, tempdiv, tempdepartment, tempnum, catcred1);
	fclose(catcred1);

	FILE *catcred2= fopen("test2.txt", "w");
	fprintf(catcred2, "Updated course credit3.5 Jaasdsadadsadasd sads asd 12 3.5\n");
	fclose(catcred2);
	assert(fileeq("test1.txt", "test2.txt"));



	//test split
	char string1[]="Hello, its me";
	char string2[10];
	int position = 0;
	assert((split(string1, position, string2))[0]=='H');
	assert((split(string1, position, string2))[3] =='l');
	assert((split(string1, position, string2))[6] ==' ');

	position =position + 5;
	assert((split(string1, position, string2))[0]=='H');
	assert((split(string1, position, string2))[1]=='e');
	assert((split(string1, position, string2))[2]=='l');
	assert((split(string1, position, string2))[3]=='l');
	assert((split(string1, position, string2))[4]=='o');



	//test compstrings
	char cstring1[]="Hello";
	char cstring2[]="Hell";
	assert(compstrings(cstring1, cstring2));

	char cstring3[]="Hello";
	char cstring4[]="Helloo";
	assert(compstrings(cstring3, cstring4)==false);

	char cstring5[]="hello";
	char cstring6[]="HeLlo";
	assert(compstrings(cstring5, cstring6));

	char cstring7[]="hello1okas";
	char cstring8[]="HeLlo1okas";
	assert(compstrings(cstring7, cstring8));

	char cstring9[]="";
	char cstring10[]="";
	assert(compstrings(cstring9, cstring10));



	//test isabsent
	char isabdiv[]="EN";
	char isabdepartment[]="600";
	char isabnum[]="491";
	assert(isabsent(catalog, counter, isabdiv, isabdepartment, isabnum)==false);

	char isabdiv1[]="AS";
	char isabdepartment1[]="600";
	char isabnum1[]="491";
	assert(isabsent(catalog, counter, isabdiv1, isabdepartment1, isabnum1));

	char isabdiv2[]="";
	char isabdepartment2[]="600";
	char isabnum2[]="491";
	assert(isabsent(catalog, counter, isabdiv2, isabdepartment2, isabnum2));

	char tempsemester[]="2019.s";
	char tempgrade[]="A+";
	struct tcourse *head=(struct tcourse*)malloc(sizeof(struct tcourse));
	head->next=NULL;
	struct tcourse *first=firstnode( catalog,counter,tempdiv,tempdepartment,tempnum, tempsemester, tempgrade);
	head->next=first;



	//test add
	char adddiv[]="EN";
	char adddepartment[]="600";
	char addnum[]="491";
	char addsemester[]="2019.s";
	char addgrade[]="A+";

	FILE *add1= fopen("test1.txt", "w");
	add(head, catalog,counter,adddiv,adddepartment,addnum, addsemester, addgrade, add1);
	ptranscript(head, add1);
	fclose(add1);

	FILE *add2= fopen("test2.txt", "w");
	fprintf(add2, "UpdatedEN.600.491 1.5 Computer Science Workshop I\n 2019.S A+\nEN.600.363 3.5 Jaasdsadadsadasd sads asd 12 3.5 2019.S A+\n");
	fclose(add2);
	assert(fileeq("test1.txt", "test2.txt"));



	//test delete
	char ddiv[]="EN";
	char ddepartment[]="600";
	char dnum[]="491";
	char dsemester[]="2019.s";

	FILE *del1= fopen("test1.txt", "w");
	delete(head, ddiv,ddepartment,dnum, dsemester, del1);
	ptranscript(head, del1);
	fclose(del1);

	FILE *del2= fopen("test2.txt", "w");
	fprintf(del2, "RemovedEN.600.363 3.5 Jaasdsadadsadasd sads asd 12 3.5 2019.S A+\n");
	fclose(del2);
	assert(fileeq("test1.txt", "test2.txt"));



	//test tpresent
	char presentdiv[]="EN";
	char presentdepartment[]="600";
	char presentnum[]="363";
	assert(tpresent(head, presentdiv, presentdepartment, presentnum));

	char presentdiv1[]="EM";
	char presentdepartment1[]="600";
	char presentnum1[]="491";
	assert(tpresent(head, presentdiv1, presentdepartment1, presentnum1)==false);



	//test tpresentadd
	char presentadddiv[]="EN";
	char presentadddepartment[]="600";
	char presentaddnum[]="363";
	char presentaddsemester[]="2019.S";
	assert(tpresentadd(head, presentadddiv, presentadddepartment, presentaddnum, presentaddsemester)==false);

	char presentadddiv1[]="EN";
	char presentadddepartment1[]="600";
	char presentaddnum1[]="491";
	char presentaddsemester1[]="2019.f";
	assert(tpresentadd(head, presentadddiv1, presentadddepartment1, presentaddnum1, presentaddsemester1)==false);



	//test tshowcourse
	char showdiv[]="EN";
	char showdepartment[]="600";
	char shownum[]="363";

	FILE *show1= fopen("test1.txt", "w");
	tshowcourse(head, showdiv, showdepartment,shownum, show1);
	fclose(show1);

	FILE *show2= fopen("test2.txt", "w");
	fprintf(show2, "2019.S A+\n");
	fclose(show2);
	assert(fileeq("test1.txt", "test2.txt"));


	//test GPA
	FILE *GPA1= fopen("test1.txt", "w");
	getGPA(head, GPA1);
	fclose(GPA1);

	FILE *GPA2= fopen("test2.txt", "w");
	fprintf(GPA2, "4.0\n");
	fclose(GPA2);
	assert(fileeq("test1.txt", "test2.txt"));


	puts("All tests successful!!!");


	fclose(fo);
	free(catalog);
	while(head != NULL){
		struct tcourse *previous=head;
		head=head->next;
		free(previous);

	}
	return 0;
}

