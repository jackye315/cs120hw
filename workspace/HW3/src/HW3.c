/*
Jack Ye
600.120
2/2/16
HW3
(917) 530-6467
jye24
jye24@jhu.edu
 */

#include "HW3.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>
#include <stdbool.h>
/*

int main(int argc, char *argv[]){

	if (argc == 1) {
		printf("Need to enter a filename!");
		return 1;  // exit program
	}

	char file[40];
	strncpy(file, argv[1],40);
	FILE *fp=fopen(file, "r");
	char infile [48];
	int counter =0;
	while(fgets(infile, 48, (FILE*)fp) != 0){
		counter++;
	}
	fclose(fp);

	FILE *fo=fopen(file, "r");


	struct ccourse *catalog = malloc(counter * (int)sizeof(struct ccourse));
		char courseinfo[100];
		int i=0;
		struct ccourse temp;
	makecatalog(fo, catalog, courseinfo, i, temp);
		char tempdiv[3];
		tempdiv[2]='\0';
		char tempdepartment[4];
		char tempnum[4];

	char tempsemester[7];
	tempsemester[6]='\0';
	char tempgrade[3];
	tempgrade[2]='\0';

	//initilize head
	struct tcourse *head=(struct tcourse*)malloc(sizeof(struct tcourse));
	head->next=NULL;

	//Menu Loop
	char command='a';
	char course[50];
	char input[50];
	char choice[]="Enter your Choice";
	int x=1;
	while(command != 'q' && command != 'Q'){
		if(x % 2){
			fprintf(stdout, "%s ", choice);
		}
		x=x+1;
		command= getchar();

		//Adding into transcript
		if(command == 'a' || command == 'A'){
			puts("Enter the course:");
			scanf("%s",course);
			puts("Enter the Semester and Grade:");
			scanf(" %[^\n]s",input);

			int i =0;
			while (course[i] == ' '){
				i++;
			}
			tempdiv[0]=toupper(course[i]);
			tempdiv[1]=toupper(course[i+1]);

			tempdepartment[0]=course[i+3];
			tempdepartment[1]=course[i+4];
			tempdepartment[2]=course[i+5];

			tempnum[0]=course[i+7];
			tempnum[1]=course[i+8];
			tempnum[2]=course[i+9];

			for(int i=0; i<6;i++){
				tempsemester[i]=input[i];
			}
			unsigned int temp=strlen(input)-2;
			tempgrade[0]=input[temp];
			tempgrade[1]=input[temp+1];
			if(head->next == NULL){
			        struct tcourse *first=firstnode(catalog,counter,tempdiv,tempdepartment,tempnum, tempsemester, tempgrade);
				head->next=first;
				puts("Updated");
			}
			else if(tpresentadd(head, tempdiv, tempdepartment,tempnum, tempsemester)){

				puts("Already present in transcript");

				}
			else{
				add(head, catalog,counter,tempdiv,tempdepartment,tempnum, tempsemester, tempgrade, stdout);
			}
		}

		//printing catalog
		if(command == 'p' || command == 'P'){
			pcatalog(catalog,counter, stdout);
		}

		//Give info
		if(command == 'c' || command =='C'){
			puts("Enter the course:");
			scanf("%s",course);
			int i =0;
			while (course[i] == ' '){
				i++;
			}
			tempdiv[0]=toupper(course[i]);
			tempdiv[1]=toupper(course[i+1]);
			tempdepartment[0]=course[i+3];
			tempdepartment[1]=course[i+4];
			tempdepartment[2]=course[i+5];
			tempnum[0]=course[i+7];
			tempnum[1]=course[i+8];
			tempnum[2]=course[i+9];
			if(isabsent(catalog,counter,tempdiv,tempdepartment,tempnum)){
				puts ("Class Absent");
			}
			else{
				catinfo(catalog,counter,tempdiv,tempdepartment,tempnum, stdout);
			}
		}

		//Update course title
		if(command == 't' || command == 'T'){
			puts("Enter the course:");
			scanf("%s",course);
			int i =0;
			while (course[i] == ' '){
				i++;
			}
			tempdiv[0]=toupper(course[i]);
			tempdiv[1]=toupper(course[i+1]);
			tempdepartment[0]=course[i+3];
			tempdepartment[1]=course[i+4];
			tempdepartment[2]=course[i+5];
			tempnum[0]=course[i+7];
			tempnum[1]=course[i+8];
			tempnum[2]=course[i+9];
			if(isabsent(catalog,counter,tempdiv,tempdepartment,tempnum)){
				puts ("Sorry, no class to updated.");
			}
			else{
				puts("Enter the New Title");
				scanf(" %[^\n]s",input);
				catuptitle(catalog, counter, tempdiv, tempdepartment, tempnum, input, stdout);
			}
		}

		//Update course credit
		if(command == 'r' || command =='R'){
			puts("Enter the course:");
			scanf("%s",course);
			int i =0;
			while (course[i] == ' '){
				i++;
			}
			tempdiv[0]=toupper(course[i]);
			tempdiv[1]=toupper(course[i+1]);
			tempdepartment[0]=course[i+3];
			tempdepartment[1]=course[i+4];
			tempdepartment[2]=course[i+5];
			tempnum[0]=course[i+7];
			tempnum[1]=course[i+8];
			tempnum[2]=course[i+9];
			if(isabsent(catalog,counter,tempdiv,tempdepartment,tempnum)){
				puts ("Sorry, no class to updated.");
			}
			else{
				puts("Enter the New Credit");
				scanf(" %[^\n]s",input);
				catupcredit(catalog, counter, tempdiv, tempdepartment, tempnum, input, stdout);
			}

		}

		//Remove course from transcript
		if(command == 'd' || command == 'D'){
			puts("Enter the course:");
			scanf("%s",course);
			int i =0;
			while (course[i] == ' '){
				i++;
			}
			tempdiv[0]=toupper(course[i]);
			tempdiv[1]=toupper(course[i+1]);
			tempdepartment[0]=course[i+3];
			tempdepartment[1]=course[i+4];
			tempdepartment[2]=course[i+5];
			tempnum[0]=course[i+7];
			tempnum[1]=course[i+8];
			tempnum[2]=course[i+9];
			puts("Enter the Semester and Grade:");
			scanf(" %[^\n]s",input);
			for(int i=0; i<6;i++){
				tempsemester[i]=input[i];
			}
			int temp=strlen(input)-2;
			tempgrade[0]=input[temp];
			tempgrade[1]=input[temp+1];
			delete(head, tempdiv,tempdepartment,tempnum, tempsemester, stdout);
		}

		//Display transcript
		if(command == 'i' || command == 'I'){
			ptranscript(head, stdout);
		}

		//Display specific course in transcript
		if(command == 'o' || command == 'O'){
			puts("Enter the course:");
			scanf("%s",course);
			int i =0;
			while (course[i] == ' '){
				i++;
			}
			tempdiv[0]=toupper(course[i]);
			tempdiv[1]=toupper(course[i+1]);
			tempdepartment[0]=course[i+3];
			tempdepartment[1]=course[i+4];
			tempdepartment[2]=course[i+5];
			tempnum[0]=course[i+7];
			tempnum[1]=course[i+8];
			tempnum[2]=course[i+9];
			if(tpresent(head, tempdiv, tempdepartment,tempnum)){
				tshowcourse(head, tempdiv, tempdepartment,tempnum, stdout);
			}
			else{
				puts("No course to show");
			}
		}

		//Display GPA
		if(command == 'g' || command == 'G'){
			getGPA(head, stdout);
		}
	}

	//Free memory
	free(catalog);
	while(head != NULL){
		struct tcourse *previous=head;
		head=head->next;
		free(previous);

	}


	fclose(fo);
	return 0;
}


*/
