/*
Jack Ye
600.120
2/2/16
HW3
(917) 530-6467
jye24
jye24@jhu.edu
 */

#include "HW3.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>
#include <stdbool.h>


void catinfo(struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[], FILE* output){
	for(int i=0;i<counter;i++){
		if(catalog[i].div[0]==tempdiv[0] && catalog[i].div[1]==tempdiv[1] && catalog[i].department==atoi(tempdepartment) && catalog[i].num==atoi(tempnum)){
			fprintf(output, "%.1f %s\n", catalog[i].assignment, catalog[i].course);
		}
	}
}

void catuptitle(struct ccourse catalog[], int counter, char tempdiv[], char tempdepartment[], char tempnum[], char newtitle[], FILE* output){
	for(int i=0;i<counter;i++){
			if(catalog[i].div[0]==tempdiv[0] && catalog[i].div[1]==tempdiv[1] && catalog[i].department==atoi(tempdepartment) && catalog[i].num==atoi(tempnum)){
				catalog[i].course[0]='\0';
				for(int x=0;x<33;x++){
					catalog[i].course[x]=newtitle[x];

				}
				fprintf(output, "Updated course title");
			}
		}
}

void catupcredit(struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[], char newcredit[], FILE* output){
	for(int i=0;i<counter;i++){
		if(catalog[i].div[0]==tempdiv[0] && catalog[i].div[1]==tempdiv[1] && catalog[i].department==atoi(tempdepartment) && catalog[i].num==atoi(tempnum)){
			catalog[i].assignment=atof(newcredit);
			fprintf(output, "Updated course credit");
		}
	}
}

bool isabsent(struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[]){
	for(int i=0;i<counter;i++){
				if(catalog[i].div[0]==tempdiv[0] && catalog[i].div[1]==tempdiv[1] && catalog[i].department==atoi(tempdepartment) && catalog[i].num==atoi(tempnum)){
					return false;
				}
	}
	return true;
}

char *split(char string[],int position, char ans[]){
	if(position == 0){
		return string;
	}
	for(int i=0;i<position;i++){
		ans[i]=string[i];
	}
	return ans;
}

bool tpresent(struct tcourse *curr, char tempdiv[], char tempdepartment[], char tempnum[]){
	while(curr->next != NULL){
		if(curr->next->basicinfo->div[0] == tempdiv[0] && curr->next->basicinfo->div[1]==tempdiv[1] && curr->next->basicinfo->department== atoi(tempdepartment) && curr->next->basicinfo->num == atoi(tempnum)){
			return true;
		}
		curr=curr->next;
	}
	return false;
}


bool tpresentadd(struct tcourse *curr, char tempdiv[], char tempdepartment[], char tempnum[], char tempsemester[]){
	while(curr->next != NULL){
		if(curr->next->basicinfo->div[0] == tempdiv[0] && curr->next->basicinfo->div[1]==tempdiv[1] && curr->next->basicinfo->department== atoi(tempdepartment) && curr->next->basicinfo->num == atoi(tempnum)){
			for(int i=0; i<9;i++){
				if(curr->next->semester[i] != tempsemester[i]){
					return false;
				}
			}
			return true;
		}
		curr=curr->next;
	}
	return false;
}

void add(struct tcourse *curr, struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[], char tempsemester[],char tempgrade[], FILE* output){
	int a=0;
	for(int i=0;i<counter;i++){
			if(catalog[i].div[0]==tempdiv[0] && catalog[i].div[1]==tempdiv[1] && catalog[i].department==atoi(tempdepartment) && catalog[i].num==atoi(tempnum)){
				a=1;
				while(curr->next != NULL){
					char year[5];
					split(curr->next->semester,4,year);
					year[4]='\0';
					char season=toupper(curr->next->semester[5]);
					char newyear[5];
					split(tempsemester,4,newyear);
					newyear[4]='\0';
					char newseason=toupper(tempsemester[5]);
					if(atoi(newyear)<atoi(year) || (atoi(newyear)==atoi(year) && newseason<=season)){
							struct tcourse *newnext=(struct tcourse*)malloc(sizeof(struct tcourse));
							newnext->basicinfo=&catalog[i];
							struct tcourse *oldnext=curr->next;
							newnext->basicinfo=&catalog[i];

							for(unsigned int i=0;i<strlen(tempsemester);i++){
								newnext->semester[i]=toupper(tempsemester[i]);
							}
							for(unsigned int i=0; i<strlen(tempgrade);i++){
								newnext->grade[i]=toupper(tempgrade[i]);
							}

							curr->next=newnext;
							newnext->next=oldnext;
							fprintf(output, "Updated");
							break;
					}
					curr=curr->next;
					}
				if(curr->next==NULL){
					struct tcourse *newnext=(struct tcourse*)malloc(sizeof(struct tcourse));
					newnext->basicinfo=&catalog[i];
					for(unsigned int i=0;i<strlen(tempsemester);i++){
						newnext->semester[i]=toupper(tempsemester[i]);
					}
					for(unsigned int i=0; i<strlen(tempgrade);i++){
						newnext->grade[i]=toupper(tempgrade[i]);
					}
					curr->next=newnext;

					fprintf(output, "Updated");
					break;
				}
			}
		}
	if(a==0){
	fprintf(output, "Invalid Input");
	}

}

struct tcourse *firstnode( struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[], char tempsemester[],char tempgrade[]){
	struct tcourse *newnext=(struct tcourse*)malloc(sizeof(struct tcourse));
	for(int i=0;i<counter;i++){
		if(catalog[i].div[0]==tempdiv[0] && catalog[i].div[1]==tempdiv[1] && catalog[i].department==atoi(tempdepartment) && catalog[i].num==atoi(tempnum)){

			newnext->basicinfo=&catalog[i];
			for(unsigned int i=0;i<strlen(tempsemester);i++){
				newnext->semester[i]=toupper(tempsemester[i]);
			}
			for(unsigned int i=0; i<strlen(tempgrade);i++){
				newnext->grade[i]=toupper(tempgrade[i]);
			}
			newnext->next=NULL;

		}
	}
	return newnext;
}

bool compstrings(char sem1[], char sem2[]){
	bool a=true;
	for(unsigned int i=0; i<strlen(sem2);i++){
		if(toupper(sem1[i])!=toupper(sem2[i])){
			a=false;
		}
	}
	return a;
}


void delete(struct tcourse *curr, char tempdiv[], char tempdepartment[], char tempnum[], char tempsemester[], FILE* output){
	int a=0;
	while(curr->next != NULL){
		if(curr->next->basicinfo->div[0]==tempdiv[0] && curr->next->basicinfo->div[1]==tempdiv[1]
		   && compstrings(curr->next->basicinfo->div, tempdiv)
		   && curr->next->basicinfo->department==atoi(tempdepartment)
		   && curr->next->basicinfo->num==atoi(tempnum)//){
		   && compstrings(curr->next->semester,tempsemester)){

			a=1;
			fprintf(output, "Removed");
			struct tcourse *trash=(struct tcourse*)malloc(sizeof(struct tcourse));
			trash=curr->next;

			curr->next=curr->next->next;
			trash->next=NULL;
			free(trash);
		}
		curr=curr->next;
	}
	if(a==0){
		fprintf(output, "Course not in transcript");
	}
}

void tshowcourse(struct tcourse *curr, char tempdiv[], char tempdepartment[], char tempnum[], FILE* output){
	while(curr->next != NULL){
		if(curr->next->basicinfo->div[0]==tempdiv[0] && curr->next->basicinfo->div[1]==tempdiv[1]
		   && compstrings(curr->next->basicinfo->div, tempdiv)
		   && curr->next->basicinfo->department==atoi(tempdepartment)
		   && curr->next->basicinfo->num==atoi(tempnum)){
			fprintf(output, "%s %s\n", curr->next->semester, curr->next->grade);
		}
		curr=curr->next;
	}
}

void getGPA(struct tcourse *curr, FILE* output){
	double GPA=0;
	double credits=0;
	double GCred=0;
	double GPACredits=0;
	double realGPA=0;
	while(curr->next != NULL){
		if(curr->next->grade[0]=='A'){
			GPA=4.0;
			credits=credits + curr->next->basicinfo->assignment;
			GCred=GPA * curr->next->basicinfo->assignment;
			GPACredits=GPACredits + GCred;
		}
		if(curr->next->grade[0]=='A' && curr->next->grade[1]=='-'){
					GPA=3.7;
					credits=credits + curr->next->basicinfo->assignment;
					GCred=GPA * curr->next->basicinfo->assignment;
					GPACredits=GPACredits + GCred;
				}
		if(curr->next->grade[0]=='B' && curr->next->grade[1]=='+'){
					GPA=3.3;
					credits=credits + curr->next->basicinfo->assignment;
					GCred=GPA * curr->next->basicinfo->assignment;
					GPACredits=GPACredits + GCred;
				}
		if(curr->next->grade[0]=='B' && curr->next->grade[1]=='/'){
							GPA=3.0;
							credits=credits + curr->next->basicinfo->assignment;
							GCred=GPA * curr->next->basicinfo->assignment;
							GPACredits=GPACredits + GCred;
				}
		if(curr->next->grade[0]=='B' && curr->next->grade[1]=='-'){
							GPA=2.7;
							credits=credits + curr->next->basicinfo->assignment;
							GCred=GPA * curr->next->basicinfo->assignment;
							GPACredits=GPACredits + GCred;
				}
		if(curr->next->grade[0]=='C' && curr->next->grade[1]=='+'){
					GPA=2.3;
					credits=credits + curr->next->basicinfo->assignment;
					GCred=GPA * curr->next->basicinfo->assignment;
					GPACredits=GPACredits + GCred;
				}
		if(curr->next->grade[0]=='C' && curr->next->grade[1]=='/'){
							GPA=2.0;
							credits=credits + curr->next->basicinfo->assignment;
							GCred=GPA * curr->next->basicinfo->assignment;
							GPACredits=GPACredits + GCred;
				}
		if(curr->next->grade[0]=='C' && curr->next->grade[1]=='-'){
							GPA=1.7;
							credits=credits + curr->next->basicinfo->assignment;
							GCred=GPA * curr->next->basicinfo->assignment;
							GPACredits=GPACredits + GCred;
				}
		if(curr->next->grade[0]=='D' && curr->next->grade[1]=='+'){
					GPA=1.3;
					credits=credits + curr->next->basicinfo->assignment;
					GCred=GPA * curr->next->basicinfo->assignment;
					GPACredits=GPACredits + GCred;
				}
		if(curr->next->grade[0]=='D' && curr->next->grade[1]=='/'){
							GPA=1.0;
							credits=credits + curr->next->basicinfo->assignment;
							GCred=GPA * curr->next->basicinfo->assignment;
							GPACredits=GPACredits + GCred;
				}
		if(curr->next->grade[0]=='F'){
							GPA=0.0;
							credits=credits + curr->next->basicinfo->assignment;
							GCred=GPA * curr->next->basicinfo->assignment;
							GPACredits=GPACredits + GCred;
				}
		curr=curr->next;
	}
	realGPA=GPACredits/credits;
	fprintf(output, "%0.1f\n",realGPA);
}

void ptranscript(struct tcourse *head, FILE* output){
	while(head->next != NULL) {
			fprintf(output, "%s.%i.%i %0.1f %s %s %s\n", head->next->basicinfo->div, head->next->basicinfo->department, head->next->basicinfo->num, head->next->basicinfo->assignment, head->next->basicinfo->course, head->next->semester, head->next->grade);
			head = head->next;
		}
}

void makecatalog(FILE *fo, struct ccourse *catalog, char courseinfo[], int i, struct ccourse temp){
	while(fgets(courseinfo, 100, (FILE*)fo) != 0){
		temp.div[0]=courseinfo[0];
		temp.div[1]=courseinfo[1];
		temp.div[2]='\0';
		char coursecode[4]={courseinfo[3],courseinfo[4],courseinfo[5]};
		temp.department= atoi(coursecode);
		char coursenum[4]={courseinfo[7],courseinfo[8],courseinfo[9]};
		temp.num= atoi(coursenum);
		char coursecred[4]={courseinfo[11],courseinfo[12],courseinfo[13]};
		temp.assignment=atof(coursecred);
		int y=0;
		for(unsigned int x=15;x<strlen(courseinfo);x++){
			temp.course[y]=courseinfo[x];
			y++;
		}
		temp.course[y]='\0';
		catalog[i]=temp;
		i++;
	}
}

void pcatalog(struct ccourse *catalog, int counter, FILE* output){
	for(int i=0; i<counter;i++){
		fprintf(output, "%s.%i.%i %.1f %s\n", catalog[i].div, catalog[i].department, catalog[i].num, catalog[i].assignment, catalog[i].course);
	}
}

int fileeq(char lhsName[], char rhsName[]) {
    FILE* lhs = fopen(lhsName, "r");
    FILE* rhs = fopen(rhsName, "r");

    // don't compare if we can't open the files
    if (lhs == NULL || rhs == NULL) return 0;

    int match = 1;
    // read until both of the files are done or there is a mismatch
    while (!feof(lhs) || !feof(rhs)) {
        if (feof(lhs) ||                  // lhs done first
            feof(rhs) ||                  // rhs done first
            (fgetc(lhs) != fgetc(rhs))) { // chars don't match
            match = 0;
            break;
        }
    }
    fclose(lhs);
    fclose(rhs);
    return match;
}


