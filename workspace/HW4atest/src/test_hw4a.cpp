/*
Jack Ye
600.120
4/3/16
HW4
(917) 530-6467
jye24
jye24@jhu.edu
 */

#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <vector>
#include <cassert>
#include "test_hw4a.hpp"

using namespace std;

int main(){

	//test print_by_alphabet
	map<string, map<string, int> > test;
	test["a"]["b"] = 1;
	test["b"]["b"] = 2;
	test["c"]["b"] = 3;
	test["a"]["c"] = 4;
	test["b"]["f"] = 5;
	ofstream file;
	file.open("test1.txt");
	print_by_alphabet(file, test);
	assert(compare_files("test1.txt", "mytest1.txt"));


	//test print_by_ralphabet
	map<string, map<string, int> > test2;
	test2["a"]["b"] = 1;
	test2["b"]["b"] = 2;
	test2["c"]["b"] = 3;
	test2["a"]["c"] = 4;
	test2["b"]["f"] = 5;
	ofstream file2;
	file2.open("test2.txt");
	print_by_ralphabet(file2, test2);
	assert(compare_files("test2.txt", "mytest2.txt"));

	//test map_real_size;
	map<string, map<string, int> > test3;
	test3["a"]["b"] = 1;
	test3["b"]["b"] = 2;
	test3["c"]["b"] = 3;
	test3["a"]["c"] = 4;
	test3["b"]["f"] = 5;
	int test3size = map_real_size(0, test3);
	assert(test3size == 5);

	map<string, map<string, int> > test4;
	int test4size = map_real_size(0, test4);
	assert(test4size == 0);

	//test print_by_frequency
	map<string, map<string, int> > test5;
	test5["a"]["b"] = 1;
	test5["b"]["b"] = 2;
	test5["c"]["b"] = 3;
	test5["c"]["c"] = 5;
	test5["b"]["f"] = 5;
	ofstream file5;
	file5.open("test5.txt");
	print_by_frequency(0,5,1,file5, test5);
	assert(compare_files("test5.txt", "mytest5.txt"));

	map<string, map<string, int> > test6;
	test6["a"]["b"] = 1;
	test6["b"]["b"] = 2;
	test6["c"]["b"] = 3;
	test6["c"]["c"] = 5;
	test6["c"]["f"] = 5;
	ofstream file6;
	file6.open("test6.txt");
	print_by_frequency(0,5,1,file6, test6);
	assert(compare_files("test6.txt", "mytest6.txt"));


	//test map_maker
	vector<string> testv7;
	testv7.push_back("hello");
	testv7.push_back("hello");
	testv7.push_back("hi");
	map<string, map<string, int> > test7;
	string data7;
	string tempdata7;
	map_maker(data7, tempdata7, testv7, test7);
	ofstream file7;
	file7.open("test7.txt");
	print_by_alphabet(file7, test7);
	assert(compare_files("test7.txt", "mytest7.txt"));

	vector<string> testv8;
	testv8.push_back("hello");
	testv8.push_back("hello");
	testv8.push_back("hi");
	testv8.push_back("hi");
	map<string, map<string, int> > test8;
	string data8;
	string tempdata8;
	map_maker(data8, tempdata8, testv8, test8);
	ofstream file8;
	file8.open("test8.txt");
	print_by_alphabet(file8, test8);
	assert(compare_files("test8.txt", "mytest8.txt"));


	cout<< "All tests completed!" << endl;

	file.close();
	file2.close();
	file5.close();
	file6.close();
	file7.close();
	file8.close();

	return 1;
}
