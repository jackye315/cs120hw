/*
Jack Ye
600.120
4/3/16
HW4
(917) 530-6467
jye24
jye24@jhu.edu
 */

#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <vector>
#include "test_hw4a.hpp"
#include <cassert>

using namespace std;

void print_by_alphabet(ofstream& file1, map<string,map<string,int> > dataholder){
	for(map<string, map<string, int> >::iterator it= dataholder.begin(); it != dataholder.end(); it++){
		for(map<string,int>::iterator i=it->second.begin();i != it->second.end();i++){
			file1 << it->first <<" "<< i->first << " "<< i->second<< endl;
		}
	}
}

void print_by_ralphabet(ofstream& file1, map<string,map<string,int> >dataholder){
	for(map<string, map<string, int> >::reverse_iterator it= dataholder.rbegin(); it != dataholder.rend(); it++){
		for(map<string,int>::iterator i=it->second.begin();i != it->second.end();i++){
			file1 << it->first <<" "<< i->first << " "<< i->second<< endl;
		}
	}
}

int map_real_size(int realsize, map<string,map<string,int> > dataholder){
	for(map<string, map<string, int> >::iterator it= dataholder.begin(); it != dataholder.end(); it++){
		for(map<string,int>::iterator i=it->second.begin();i != it->second.end();i++){
			realsize=realsize+1;
		}
	}
	return realsize;
}

void print_by_frequency(int size, int realsize, int counter, ofstream& file1, map<string,map<string,int> > dataholder){
	  while(size<realsize){
		  //int counter=1;
		  for(map<string, map<string, int> >::iterator it= dataholder.begin(); it != dataholder.end(); it++){
			  for(map<string,int>::iterator i=it->second.begin();i != it->second.end();i++){
				  if(i->second == counter){
					  file1 << it->first <<" "<< i->first << " "<< i->second<< endl;
					  size=size+1;
				  }
			  }
		  //file1 << it->first << it->second.first << it->second.second;
		  //file1 << endl;
	  	  }
		  counter=counter+1;
	  }
}

void map_maker(string data, string tempdata, vector<string> filewords, map<string, map<string, int> >& dataholder){
	  for(unsigned int i=0; i<filewords.size()-1; i++){
		  data=filewords.at(i);
		  tempdata=filewords.at(i+1);
		  int x=1;
		  for(map<string, map<string, int> >::iterator it= dataholder.begin(); it != dataholder.end(); it++){
			  if(it->first == data){
				  for(map<string,int>::iterator i=it->second.begin();i != it->second.end();i++){
					  if(i->first == tempdata){
						  x=i->second + 1;
						  //dataholder[data][tempdata] = x;
					  }
				  }
			  }
		  }
		  dataholder[data][tempdata] = x;
	  }
}

bool compare_files(std::string f1, std::string f2)
{
    std::ifstream is1(f1), is2(f2);
    if(!is1.is_open() || !is2.is_open()) {
        std::cerr << "could not open one of the files" << std::endl;
        return false;
    }
    char c1, c2;
    while(is1.get(c1)) {
        is2.get(c2);
        if(c1 != c2) {
            return false;
        }
    }
    is2.get(c2);
    return is1.eof() == is2.eof();
}
