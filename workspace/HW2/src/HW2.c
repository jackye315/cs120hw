/*
Jack Ye
600.120
2/2/16
HW2
(917) 530-6467
jye24
jye24@jhu.edu
 */

#include "dnasearch.h"


int main(int argc, char *argv[]){
	if (argc == 1) {
		printf("Need to enter a filename!");
		return 1;  // exit program
	}

	char file[40];
	strncpy(file, argv[1],40);
	FILE *fp=fopen(file, "r");

	//Writes DNA sequence from file into array
	char DNAlib[15000];
	//int x=0;
	txtwriter(fp, DNAlib);
	puts(DNAlib);
	fclose(fp);

	if(strlen(DNAlib)==1){
		puts("Text invalid");
		return 1;
	}

	//loop that writes in user input, then compares with txt
	char DNApat[151000];
	printf("Enter a pattern to check file!\n");
	while(fscanf(stdin,"%s",&DNApat[0]) != EOF){
		patCheck(DNApat,DNAlib);
		printf("%s ", DNApat);

		int j=0;
		int ans[15000];
		int match =0;
		compare(DNApat, DNAlib, ans, &j, &match);
		if(match ==0){
			puts("No match found\n");
		}
		else{
			for(int i=0; i<j;i++){
				printf("%i ",ans[i]);
			}
			puts("\n");
		}
	}
	return 0;
}
