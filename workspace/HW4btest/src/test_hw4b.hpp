/*
Jack Ye
600.120
4/3/16
HW4
(917) 530-6467
jye24
jye24@jhu.edu
 */

#ifndef TEST_HW4B_HPP_
#define TEST_HW4B_HPP_



#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <vector>
#include <cstdlib>

using namespace std;

void print_sentence(int command, vector<string> phrasebank, ofstream& cout);
bool compare_files(std::string f1, std::string f2);

#endif /* TEST_HW4B_HPP_ */
