/*
Jack Ye
600.120
4/3/16
HW4
(917) 530-6467
jye24
jye24@jhu.edu
 */


#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <vector>
#include <cstdlib>
#include "test_hw4b.hpp"
#include <cassert>

using namespace std;

void print_sentence(int command, vector<string> phrasebank, ofstream& cout){
	while(command != 0){
		for(unsigned int i=0; i<phrasebank.size();i++){
			if(phrasebank.at(i).find("<START>") != string::npos){
				cout<<phrasebank.at(i)<< " ";
			}
		}
		int randomnum=rand() % phrasebank.size();
		while(phrasebank.at(randomnum).find("<END>") == string::npos){
			if(phrasebank.at(randomnum).find("<START>") != string::npos){

			}
			else{
				cout<<phrasebank.at(randomnum)<< " ";
			}
			randomnum=rand() % phrasebank.size();

		}
		cout<<phrasebank.at(randomnum) << endl;
		command--;
	}
}


bool compare_files(std::string f1, std::string f2)
{
    std::ifstream is1(f1), is2(f2);
    if(!is1.is_open() || !is2.is_open()) {
        std::cerr << "could not open one of the files" << std::endl;
        return false;
    }
    char c1, c2;
    while(is1.get(c1)) {
        is2.get(c2);
        if(c1 != c2) {
            return false;
        }
    }
    is2.get(c2);
    return is1.eof() == is2.eof();
}
