/*
Jack Ye
600.120
4/3/16
HW4
(917) 530-6467
jye24
jye24@jhu.edu
 */


#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <vector>
#include <cstdlib>
#include "hw4b.hpp"
#include <cassert>

using namespace std;

int main(){


	//test print_sentence

	vector<string> testv1;
	testv1.push_back("hello");
	testv1.push_back("<START>");
	testv1.push_back("hello");
	testv1.push_back("hello");
	testv1.push_back("hello");
	testv1.push_back("hello");
	testv1.push_back("hello");
	testv1.push_back("hello");
	testv1.push_back("hi");
	testv1.push_back("<END>");
	ofstream file;
	file.open("testb.txt");
	print_sentence(3, testv1, file);
	file.close();
	ifstream file1;
	file1.open("testb.txt");
	string test1;
	int counter_hello=0;
	int counter_hi=0;
	while(file1 >> test1){
		if(test1 == "hello"){
			counter_hello++;
		}
		if(test1 == "hi"){
			counter_hi++;
		}

	}
	assert(counter_hello > counter_hi);
	cout<< "All tests passed!"<< endl;
	return 1;
}
