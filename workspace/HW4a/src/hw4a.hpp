/*
Jack Ye
600.120
4/3/16
HW4
(917) 530-6467
jye24
jye24@jhu.edu
 */

#ifndef HW4A_HPP_
#define HW4A_HPP_
#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <vector>
#include <cassert>


using namespace std;

void print_by_alphabet(ofstream& file1, map<string,map<string,int> > dataholder);
void print_by_ralphabet(ofstream& file1, map<string,map<string,int> >dataholder);
int map_real_size(int realsize, map<string,map<string,int> > dataholder);
void print_by_frequency(int size, int realsize, int counter, ofstream& file1, map<string,map<string,int> > dataholder);
void map_maker(string data, string tempdata, vector<string> filewords, map<string, map<string, int> >& dataholder);
bool compare_files(std::string f1, std::string f2);



#endif /* HW4A_HPP_ */
