/*
Jack Ye
600.120
4/3/16
HW4
(917) 530-6467
jye24
jye24@jhu.edu
 */

#include "hw4a.hpp"

using namespace std;

int main(int argc, char *argv[]){
	if (argc < 2) {
		cout << "Need to enter command and filename!" << endl;
		return 1;  // exit program
	}

	string command = argv[1];
	if(command.compare("a") != 0 && command.compare("r") != 0 && command.compare("c") != 0 && command.compare("A") != 0 && command.compare("R") != 0 && command.compare("C") !=0){// || argv[1] != "r" || argv[1] != "c" || argv[1] != "A" || argv[1] != "R" || argv[1] != "C" ){
		cout << "Need to enter correct command!" << endl;
		return 1;
	}

  string name="hw4text.txt";

  ofstream file1;
  string data;
  string tempdata;
  map<string, map<string,int> > dataholder;
  vector<string> filewords;

  file1.open("newtext.txt");

  //put data into vector
  while(cin >> tempdata){
    filewords.push_back(tempdata);
  }

  //create map
  map_maker(data,tempdata, filewords, dataholder);

  //print alphabetically
  if(command.compare("a") == 0 || command.compare("A") == 0){
     print_by_alphabet(file1, dataholder);
     cout<<"Wrote to newtext.txt"<<endl;
  }

  //print reverse alphabetically
  else  if(command.compare("r") == 0 || command.compare("R") == 0){
  print_by_ralphabet(file1, dataholder);
  cout<<"Wrote to newtext.txt"<<endl;
  }

  //print by frequency
  else if(command.compare("c") == 0 || command.compare("C") == 0){
	  int realsize = map_real_size(0,dataholder);
	  int size=0;
	  int counter=1;
	  print_by_frequency(size, realsize, counter, file1, dataholder);
	  cout<<"Wrote to newtext.txt"<<endl;
  }

  file1.close();
  return 1;
}
