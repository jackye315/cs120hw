/*
Jack Ye
600.120
4/3/16
HW4
(917) 530-6467
jye24
jye24@jhu.edu
 */



#include "hw4b.hpp"


using namespace std;

void print_sentence(int command, vector<pair<string,string> > phrasebank, ofstream& cout){
	string previous;
  while(command != 0){
    bool finish_sentence = false;
    for(unsigned int i=0; i<phrasebank.size();i++){
      if(phrasebank.at(i).first =="<START>"){
	cout<<phrasebank.at(i).first << " " << phrasebank.at(i).second << " ";
	previous = phrasebank.at(i).second;
      }
    }
    int randomnum=rand() % phrasebank.size();
    while(!finish_sentence){
      if(phrasebank.at(randomnum).first=="<START>"){

      }
      else if(phrasebank.at(randomnum).first == previous){
	cout<<phrasebank.at(randomnum).second<< " ";
	previous = phrasebank.at(randomnum).second;
	if(phrasebank.at(randomnum).second == "<END>"){
	  finish_sentence=true;
	}
      }
      randomnum=rand() % phrasebank.size();
    }
    cout << endl;
    command--;
  }
}

void print_sentence(int command, vector<pair<string, string> > phrasebank){
  string previous;
  while(command != 0){
    bool finish_sentence = false;
    for(unsigned int i=0; i<phrasebank.size();i++){
      if(phrasebank.at(i).first =="<START>"){
	cout<<phrasebank.at(i).first << " " << phrasebank.at(i).second << " ";
	previous = phrasebank.at(i).second;
      }
    }
    int randomnum=rand() % phrasebank.size();
    while(!finish_sentence){
      if(phrasebank.at(randomnum).first=="<START>"){

      }
      else if(phrasebank.at(randomnum).first == previous){
	cout<<phrasebank.at(randomnum).second<< " ";
	previous = phrasebank.at(randomnum).second;
	if(phrasebank.at(randomnum).second == "<END>"){
	  finish_sentence=true;
	}
      }
      randomnum=rand() % phrasebank.size();
    }
    cout << endl;
    command--;
  }
}


bool compare_files(std::string f1, std::string f2)
{
    std::ifstream is1(f1), is2(f2);
    if(!is1.is_open() || !is2.is_open()) {
        std::cerr << "could not open one of the files" << std::endl;
        return false;
    }
    char c1, c2;
    while(is1.get(c1)) {
        is2.get(c2);
        if(c1 != c2) {
            return false;
        }
    }
    is2.get(c2);
    return is1.eof() == is2.eof();
}
