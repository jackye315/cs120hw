/*
Jack Ye
600.120
4/3/16
HW4
(917) 530-6467
jye24
jye24@jhu.edu
 */


#include "hw4b.hpp"

using namespace std;

int main(int argc, char *argv[]){
	if (argc < 2) {
		cout << "Need to enter command and filename!" << endl;
		return 1;  // exit program
	}

	int command = stoi(argv[1]);
	if(command < 0){
		cout << "Need to enter correct command!" << endl;
		return 1;
	}
	srand(1);
	string data1;
	string data2;
	int data3;
	vector<pair<string, string> > phrasebank;

	//create vector to hold all phrases
	while(cin >> data1){
	  cin >> data2;
	  cin >> data3;
	  while(data3 != 0){
	    phrasebank.push_back(make_pair(data1, data2));
	    data3=data3-1;
	  }
	}

	//generate and print the random sentences
	print_sentence(command, phrasebank);
	return 1;
}
