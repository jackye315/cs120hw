/*
Jack Ye
600.120
2/2/16
HW0
(917) 530-6467
jye24
jye24@jhu.edu
 */

#include <stdio.h>
#include <string.h>  // for string functions:

#define LENGTH 16

int main(int argc, char *argv[]) {
	//char c[]="EN.600.120B+3.5";

	  char c[LENGTH];
	  if (argc == 1) {
	    printf("Usage: hw0 XX.###.###Gg#.#\n");
	    return 1;  // exit program
	  }

	  strncpy(c, argv[1], LENGTH);  // copy to course string
	  c[LENGTH-1] = '\0';   // make sure last character is null
	  printf("course string: %s\n", c);  // echo input

	//School
	if(c[0]=='A'){
		puts("Division: 0");
	}
	else if(c[0]== 'B'){
		puts("Division: 1");
	}
	else if(c[0]== 'E' && c[1]== 'D'){
		puts("Division: 2");
	}
	else if(c[0]== 'E' && c[1]== 'N'){
		puts("Division: 3");
	}
	else if(c[0]== 'M'){
		puts("Division: 4");
	}
	else if(c[0]== 'P' && c[1]== 'H'){
		puts("Division: 5");
	}
	else if(c[0]== 'P' && c[1]== 'Y'){
		puts("Division: 6");
	}
	else if(c[0]== 'S'){
		puts("Division: 7");
	}

	//Department
	printf("Department: %c%c%c \n",c[3],c[4],c[5]);

	//Course
	printf("Course: %c%c%c \n",c[7],c[8],c[9]);
	//Grade
	if(c[10]=='A' && c[11]=='+'){
		puts("Grade: 0 0");
	}
	if(c[10]=='A' && c[11]=='-'){
		puts("Grade: 0 1");
	}
	if(c[10]=='A' && c[11]=='/'){
		puts("Grade: 0 2");
	}
	if(c[10]=='B' && c[11]=='+'){
		puts("Grade: 1 0");
	}
	if(c[10]=='B' && c[11]=='-'){
		puts("Grade: 1 1");
	}
	if(c[10]=='B' && c[11]=='/'){
		puts("Grade: 1 2");
	}
	if(c[10]=='C' && c[11]=='+'){
		puts("Grade: 2 0");
	}
	if(c[10]=='C' && c[11]=='-'){
		puts("Grade: 2 1");
	}
	if(c[10]=='C' && c[11]=='/'){
		puts("Grade: 2 2");
	}
	if(c[10]=='D' && c[11]=='+'){
		puts("Grade: 3 0");
	}
	if(c[10]=='D' && c[11]=='-'){
		puts("Grade: 3 1");
	}
	if(c[10]=='D' && c[11]=='/'){
		puts("Grade: 3 2");
	}
	if(c[10]=='F' && c[11]=='+'){
		puts("Grade: 4 0");
	}
	if(c[10]=='F' && c[11]=='-'){
		puts("Grade: 4 1");
	}
	if(c[10]=='F' && c[11]=='/'){
		puts("Grade: 4 2");
	}
	if(c[10]=='I' && c[11]=='+'){
		puts("Grade: 5 0");
	}
	if(c[10]=='I' && c[11]=='-'){
		puts("Grade: 5 1");
	}
	if(c[10]=='I' && c[11]=='/'){
		puts("Grade: 5 2");
	}
	if(c[10]=='S' && c[11]=='+'){
		puts("Grade: 6 0");
	}
	if(c[10]=='S' && c[11]=='-'){
		puts("Grade: 6 1");
	}
	if(c[10]=='S' && c[11]=='/'){
		puts("Grade: 6 2");
	}
	if(c[10]=='U' && c[11]=='+'){
		puts("Grade: 7 0");
	}
	if(c[10]=='U' && c[11]=='-'){
		puts("Grade: 7 1");
	}
	if(c[10]=='U' && c[11]=='/'){
		puts("Grade: 7 2");
	}

	//Credits
	if(c[12]=='0' && c[14]=='0'){
		puts("Credits: 0 0");
	}
	else if(c[12]=='0' && c[14]=='5'){
		puts("Credits: 0 1");
	}
	else if(c[12]=='1' && c[14]=='0'){
		puts("Credits: 1 0");
	}
	else if(c[12]=='1' && c[14]=='5'){
		puts("Credits: 1 1");
	}
	else if(c[12]=='2' && c[14]=='0'){
		puts("Credits: 2 0");
	}
	else if(c[12]=='2' && c[14]=='5'){
		puts("Credits: 2 1");
	}
	else if(c[12]=='3' && c[14]=='0'){
		puts("Credits: 3 0");
	}
	else if(c[12]=='3' && c[14]=='5'){
		puts("Credits: 3 1");
	}
	else if(c[12]=='4' && c[14]=='0'){
		puts("Credits: 4 0");
	}
	else if(c[12]=='4' && c[14]=='5'){
		puts("Credits: 4 1");
	}
	else if(c[12]=='5' && c[14]=='0'){
		puts("Credits: 5 0");
	}
	else if(c[12]=='5' && c[14]=='5'){
		puts("Credits: 5 1");
	}

	return 0;


}
