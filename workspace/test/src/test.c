/*
 * test.c
 *
 *  Created on: Mar 6, 2016
 *      Author: JackYe
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>

int fileeq(char lhsName[], char rhsName[]) {
    FILE* lhs = fopen(lhsName, "r");
    FILE* rhs = fopen(rhsName, "r");

    // don't compare if we can't open the files
    if (lhs == NULL || rhs == NULL) return 0;

    int match = 1;
    // read until both of the files are done or there is a mismatch
    while (!feof(lhs) || !feof(rhs)) {
        if (feof(lhs) ||                  // lhs done first
            feof(rhs) ||                  // rhs done first
            (fgetc(lhs) != fgetc(rhs))) { // chars don't match
            match = 0;
            break;
        }
    }
    fclose(lhs);
    fclose(rhs);
    return match;
}


void dothis(FILE* output, char *words){
	fprintf(output, "%s", words);
}

int main(){

	FILE *fp= fopen("test1.txt", "w");
	char words[]="I think this will work";
	dothis(fp, words);
	//fprintf(fp, "I think this will work");
	fclose(fp);

	FILE *fo= fopen("test2.txt", "w");
	fprintf(fo, "I think this will work");
	fclose(fo);

	assert(fileeq("test1.txt", "test2.txt"));
	puts("HI");

	//char words[]="Hello, its me!";
	dothis(stdout, words);

}

