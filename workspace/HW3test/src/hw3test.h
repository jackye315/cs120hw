/*
 * hw3test.h
 *
 *  Created on: Mar 9, 2016
 *      Author: JackYe
 */

#ifndef HW3TEST_H_
#define HW3TEST_H_

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>

struct ccourse{
	char div[2];
	int department;
	int num;
	float assignment;
	char course[33];
};

struct tcourse{
	/*
	char div[2];
	int department;
	int num;
	float assignment;
	char course[33];
	*/
	struct ccourse *basicinfo;
	char semester[7];
	char grade[3];
	struct tcourse *next;
};

void makecatalog(FILE *fo, int counter, struct ccourse *catalog, char courseinfo[], int i, struct ccourse temp);
void pcatalog(struct ccourse *catalog, int counter);
void catinfo(struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[]);
void catuptitle(struct ccourse catalog[], int counter, char tempdiv[], char tempdepartment[], char tempnum[], char newtitle[]);
void catupcredit(struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[], char newcredit[]);
char *split(char string[],int position, char ans[]);
void add(struct tcourse *curr, struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[], char tempsemester[],char tempgrade[]);
struct tcourse *firstnode(struct tcourse *curr, struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[], char tempsemester[],char tempgrade[]);
bool compstrings(char sem1[], char sem2[]);
bool isabsent(struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[]);
void delete(struct tcourse *curr, char tempdiv[], char tempdepartment[], char tempnum[], char tempsemester[]);
void getGPA(struct tcourse *curr);
void ptranscript(struct tcourse *head);
bool tpresent(struct tcourse *curr, char tempdiv[], char tempdepartment[], char tempnum[]);
void tshowcourse(struct tcourse *curr, char tempdiv[], char tempdepartment[], char tempnum[]);
bool tpresentadd(struct tcourse *curr, char tempdiv[], char tempdepartment[], char tempnum[], char tempsemester[]);



#endif /* HW3TEST_H_ */
