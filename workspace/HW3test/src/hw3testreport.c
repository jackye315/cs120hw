/*
Jack Ye
600.120
2/2/16
HW3
(917) 530-6467
jye24
jye24@jhu.edu
 */

#include "hw3test.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>
#include <stdbool.h>


/*
struct ccourse{
	char div[2];
	int department;
	int num;
	float assignment;
	char course[33];
};

struct tcourse{

	char div[2];
	int department;
	int num;
	float assignment;
	char course[33];

	struct ccourse basicinfo;
	char semester[7];
	char grade[3];
	struct tcourse *next;
};
*/
void catinfo(struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[]){
	for(int i=0;i<counter;i++){
		if(catalog[i].div[0]==tempdiv[0] && catalog[i].div[1]==tempdiv[1] && catalog[i].department==atoi(tempdepartment) && catalog[i].num==atoi(tempnum)){
			printf("%.1f %s\n", catalog[i].assignment, catalog[i].course);
			//break;
		}
	}
	//puts("Class Absent");
}

void catuptitle(struct ccourse catalog[], int counter, char tempdiv[], char tempdepartment[], char tempnum[], char newtitle[]){
	for(int i=0;i<counter;i++){
			if(catalog[i].div[0]==tempdiv[0] && catalog[i].div[1]==tempdiv[1] && catalog[i].department==atoi(tempdepartment) && catalog[i].num==atoi(tempnum)){
				catalog[i].course[0]='\0';
				for(int x=0;x<33;x++){
					catalog[i].course[x]=newtitle[x];

				}
				puts("Updated course title");
				//printf("%.1f %s\n", catalog[i].assignment, catalog[i].course);
				//break;
				//catalog[i].course[x]='\0';
			}
		}
	//puts("Sorry, no class to updated.");
}

void catupcredit(struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[], char newcredit[]){
	for(int i=0;i<counter;i++){
		if(catalog[i].div[0]==tempdiv[0] && catalog[i].div[1]==tempdiv[1] && catalog[i].department==atoi(tempdepartment) && catalog[i].num==atoi(tempnum)){
			catalog[i].assignment=atof(newcredit);
			puts("Updated course credit");
			//printf("%.1f %s\n", catalog[i].assignment, catalog[i].course);
		}
	}
}

bool isabsent(struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[]){
	for(int i=0;i<counter;i++){
				if(catalog[i].div[0]==tempdiv[0] && catalog[i].div[1]==tempdiv[1] && catalog[i].department==atoi(tempdepartment) && catalog[i].num==atoi(tempnum)){
					return false;
				}
	}
	return true;
}

char *split(char string[],int position, char ans[]){
	if(position==0 || position>strlen(string)){
		return string;
	}
	else{
		for(int i=0;i<position;i++){
			ans[i]=string[i];
		}
		return ans;
	}
}

bool tpresent(struct tcourse *curr, char tempdiv[], char tempdepartment[], char tempnum[]){
	while(curr->next != NULL){
		if(curr->next->basicinfo->div[0] == tempdiv[0] && curr->next->basicinfo->div[1]==tempdiv[1] && curr->next->basicinfo->department== atoi(tempdepartment) && curr->next->basicinfo->num == atoi(tempnum)){
			return true;
		}
		curr=curr->next;
	}
	return false;
}


bool tpresentadd(struct tcourse *curr, char tempdiv[], char tempdepartment[], char tempnum[], char tempsemester[]){
	while(curr->next != NULL){
		if(curr->next->basicinfo->div[0] == tempdiv[0] && curr->next->basicinfo->div[1]==tempdiv[1] && curr->next->basicinfo->department== atoi(tempdepartment) && curr->next->basicinfo->num == atoi(tempnum)){
			for(int i=0; i<9;i++){
				if(curr->next->semester[i] != tempsemester[i]){
					return false;
				}
			}
			return true;
			//return true;
		}
		curr=curr->next;
	}
	return false;
}

void add(struct tcourse *curr, struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[], char tempsemester[],char tempgrade[]){
	int a=0;
	for(int i=0;i<counter;i++){
			if(catalog[i].div[0]==tempdiv[0] && catalog[i].div[1]==tempdiv[1] && catalog[i].department==atoi(tempdepartment) && catalog[i].num==atoi(tempnum)){
				//puts("Updated");
				/*
				if(curr->next == NULL){

					struct tcourse *newnext=(struct tcourse*)malloc(sizeof(struct tcourse));
					newnext->basicinfo=catalog[i];
					for(int i=0;i<strlen(tempsemester);i++){
						newnext->semester[i]=toupper(tempsemester[i]);
					}
					for(int i=0; i<strlen(tempgrade);i++){
						newnext->grade[i]=toupper(tempgrade[i]);
					}
					newnext->next=NULL;
					curr->next = newnext;
					break;
					//puts("Failed");
				}
				*/
				a=1;
				while(curr->next != NULL){
					//printf("Updated");
					char year[5];
					split(curr->next->semester,4,year);
					year[4]='\0';
					char season=toupper(curr->next->semester[5]);
					//putchar(season);
					char newyear[5];
					split(tempsemester,4,newyear);
					newyear[4]='\0';
					char newseason=toupper(tempsemester[5]);
//printf("%s %s\n",newyear, year);
//printf("%c %c",newseason, season);
					//printf("%s %i %i", curr->next->basicinfo->div, curr->next->basicinfo->department, curr->next->basicinfo->num);
					//printf("tempdiv: %s", tempdiv);
					/*
					if(curr->next->basicinfo->div[0] == tempdiv[0] && curr->next->basicinfo->div[1]==tempdiv[1] && curr->next->basicinfo->department== atoi(tempdepartment) && curr->next->basicinfo->num == atoi(tempnum)){
						puts("Already present in transcript");
						break;
					}
					*/

					if(atoi(newyear)<atoi(year) || (atoi(newyear)==atoi(year) && newseason<=season)){
							//printf("TRY");
							struct tcourse *newnext=(struct tcourse*)malloc(sizeof(struct tcourse));
							newnext->basicinfo=&catalog[i];
							struct tcourse *oldnext=curr->next;
							//printf("%s\n",oldnext->semester);

							newnext->basicinfo=&catalog[i];

							for(int i=0;i<strlen(tempsemester);i++){
								newnext->semester[i]=toupper(tempsemester[i]);
							}
							//printf("%s\n",newnext->semester);
							for(int i=0; i<strlen(tempgrade);i++){
								newnext->grade[i]=toupper(tempgrade[i]);
							}

							curr->next=newnext;
//printf("%s\n", curr->next->semester);
							newnext->next=oldnext;
							//printf("%s\n", newnext->next->semester);
							puts("Updated");
							break;

					}


					curr=curr->next;
					}

				if(curr->next==NULL){
					struct tcourse *newnext=(struct tcourse*)malloc(sizeof(struct tcourse));
					newnext->basicinfo=&catalog[i];
					for(int i=0;i<strlen(tempsemester);i++){
						newnext->semester[i]=toupper(tempsemester[i]);
					}
					for(int i=0; i<strlen(tempgrade);i++){
						newnext->grade[i]=toupper(tempgrade[i]);
					}
					curr->next=newnext;

					puts("Updated");
					break;
				}
				//puts("Updated");

			}

		}
	if(a==0){
	puts("Invalid Input");
	}

}

struct tcourse *firstnode(struct tcourse *curr, struct ccourse catalog[],int counter,char tempdiv[], char tempdepartment[], char tempnum[], char tempsemester[],char tempgrade[]){
	struct tcourse *newnext=(struct tcourse*)malloc(sizeof(struct tcourse));
	for(int i=0;i<counter;i++){
		if(catalog[i].div[0]==tempdiv[0] && catalog[i].div[1]==tempdiv[1] && catalog[i].department==atoi(tempdepartment) && catalog[i].num==atoi(tempnum)){

			newnext->basicinfo=&catalog[i];
			for(int i=0;i<strlen(tempsemester);i++){
				newnext->semester[i]=toupper(tempsemester[i]);
			}
			for(int i=0; i<strlen(tempgrade);i++){
				newnext->grade[i]=toupper(tempgrade[i]);
			}
			newnext->next=NULL;

		}
	}
	return newnext;
}

bool compstrings(char sem1[], char sem2[]){
	bool a=true;
	for(int i=0; i<strlen(sem2);i++){
		if(toupper(sem1[i])!=toupper(sem2[i])){
			a=false;
		}
	}
	return a;
}


void delete(struct tcourse *curr, char tempdiv[], char tempdepartment[], char tempnum[], char tempsemester[]){
	int a=0;
	//puts("ji");
	while(curr->next != NULL){
		//puts("ii");
		//tempsemester=toupper(tempsemester);
		//printf("%s %s\n", curr->next->semester, tempsemester);
		if(curr->next->basicinfo->div[0]==tempdiv[0] && curr->next->basicinfo->div[1]==tempdiv[1]
		   && compstrings(curr->next->basicinfo->div, tempdiv)
		   && curr->next->basicinfo->department==atoi(tempdepartment)
		   && curr->next->basicinfo->num==atoi(tempnum)//){
		   && compstrings(curr->next->semester,tempsemester)){
		   //&& curr->next->semester==tempsemester){

			a=1;
			//puts("hello");
			puts("Removed");
			struct tcourse *trash=(struct tcourse*)malloc(sizeof(struct tcourse));
			trash=curr->next;

			curr->next=curr->next->next;
			trash->next=NULL;
			free(trash);
			//a=1;


		}
		curr=curr->next;
	}
	if(a==0){
		puts("Course not in transcript");
	}
}

void tshowcourse(struct tcourse *curr, char tempdiv[], char tempdepartment[], char tempnum[]){
	while(curr->next != NULL){
		if(curr->next->basicinfo->div[0]==tempdiv[0] && curr->next->basicinfo->div[1]==tempdiv[1]
		   && compstrings(curr->next->basicinfo->div, tempdiv)
		   && curr->next->basicinfo->department==atoi(tempdepartment)
		   && curr->next->basicinfo->num==atoi(tempnum)){
			printf("%s %s\n", curr->next->semester, curr->next->grade);
		}
		curr=curr->next;
	}
}

void getGPA(struct tcourse *curr){
	double GPA=0;
	double credits=0;
	double GCred=0;
	double GPACredits=0;
	double realGPA=0;
	while(curr->next != NULL){
		//puts("JI");
		//putchar(curr->next->grade[0]);
		if(curr->next->grade[0]=='A'){
			GPA=4.0;
			credits=credits + curr->next->basicinfo->assignment;
			GCred=GPA * curr->next->basicinfo->assignment;
			GPACredits=GPACredits + GCred;


		}
		if(curr->next->grade[0]=='A' && curr->next->grade[1]=='-'){
					GPA=3.7;
					credits=credits + curr->next->basicinfo->assignment;
					GCred=GPA * curr->next->basicinfo->assignment;
					GPACredits=GPACredits + GCred;


				}
		if(curr->next->grade[0]=='B' && curr->next->grade[1]=='+'){
					GPA=3.3;
					credits=credits + curr->next->basicinfo->assignment;
					GCred=GPA * curr->next->basicinfo->assignment;
					GPACredits=GPACredits + GCred;


				}
		if(curr->next->grade[0]=='B' && curr->next->grade[1]=='/'){
							GPA=3.0;
							credits=credits + curr->next->basicinfo->assignment;
							GCred=GPA * curr->next->basicinfo->assignment;
							GPACredits=GPACredits + GCred;


				}
		if(curr->next->grade[0]=='B' && curr->next->grade[1]=='-'){
							GPA=2.7;
							credits=credits + curr->next->basicinfo->assignment;
							GCred=GPA * curr->next->basicinfo->assignment;
							GPACredits=GPACredits + GCred;


				}
		if(curr->next->grade[0]=='C' && curr->next->grade[1]=='+'){
					GPA=2.3;
					credits=credits + curr->next->basicinfo->assignment;
					GCred=GPA * curr->next->basicinfo->assignment;
					GPACredits=GPACredits + GCred;


				}
		if(curr->next->grade[0]=='C' && curr->next->grade[1]=='/'){
							GPA=2.0;
							credits=credits + curr->next->basicinfo->assignment;
							GCred=GPA * curr->next->basicinfo->assignment;
							GPACredits=GPACredits + GCred;


				}
		if(curr->next->grade[0]=='C' && curr->next->grade[1]=='-'){
							GPA=1.7;
							credits=credits + curr->next->basicinfo->assignment;
							GCred=GPA * curr->next->basicinfo->assignment;
							GPACredits=GPACredits + GCred;


				}
		if(curr->next->grade[0]=='D' && curr->next->grade[1]=='+'){
					GPA=1.3;
					credits=credits + curr->next->basicinfo->assignment;
					GCred=GPA * curr->next->basicinfo->assignment;
					GPACredits=GPACredits + GCred;


				}
		if(curr->next->grade[0]=='D' && curr->next->grade[1]=='/'){
							GPA=1.0;
							credits=credits + curr->next->basicinfo->assignment;
							GCred=GPA * curr->next->basicinfo->assignment;
							GPACredits=GPACredits + GCred;


				}
		if(curr->next->grade[0]=='F'){
							GPA=0.0;
							credits=credits + curr->next->basicinfo->assignment;
							GCred=GPA * curr->next->basicinfo->assignment;
							GPACredits=GPACredits + GCred;


				}

		curr=curr->next;
	}
	realGPA=GPACredits/credits;
	printf("%0.1f\n",realGPA);
}

void ptranscript(struct tcourse *head){
	while(head->next != NULL) {
			printf("%s.%i.%i %0.1f %s %s %s\n", head->next->basicinfo->div, head->next->basicinfo->department, head->next->basicinfo->num, head->next->basicinfo->assignment, head->next->basicinfo->course, head->next->semester, head->next->grade);
			head = head->next;
		}
}

void makecatalog(FILE *fo, int counter, struct ccourse *catalog, char courseinfo[], int i, struct ccourse temp){
	//struct ccourse temp;
	while(fgets(courseinfo, 100, (FILE*)fo) != 0){

		temp.div[0]=courseinfo[0];
		temp.div[1]=courseinfo[1];
		char coursecode[4]={courseinfo[3],courseinfo[4],courseinfo[5]};
		temp.department= atoi(coursecode);
		//printf("%i ", temp.department);
		char coursenum[4]={courseinfo[7],courseinfo[8],courseinfo[9]};
		//puts(coursenum);
		temp.num= atoi(coursenum);
		//printf("%i", temp.num);
		char coursecred[4]={courseinfo[11],courseinfo[12],courseinfo[13]};
		temp.assignment=atof(coursecred);
		//printf(" %.1f\n", temp.assignment);
		int y=0;
		//printf("%i\n",strlen(courseinfo));
		for(int x=15;x<strlen(courseinfo);x++){
			temp.course[y]=courseinfo[x];
			//putchar(temp.course[y]);
			y++;
		}
		temp.course[y]='\0';
		//puts(temp.course);
		catalog[i]=temp;
		//puts(temp.course);
		//puts(courseinfo);
		//printf("%s.%i.%i %.1f %s\n", temp.div, temp.department, temp.num, temp.assignment, temp.course);
		i++;
	}
}

void pcatalog(struct ccourse *catalog, int counter){
	for(int i=0; i<counter;i++){
		printf("%s.%i.%i %.1f %s\n", catalog[i].div, catalog[i].department, catalog[i].num, catalog[i].assignment, catalog[i].course);
	}
}
