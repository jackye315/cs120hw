/*
Jack Ye
600.120
2/2/16
HW2
(917) 530-6467
jye24
jye24@jhu.edu
 */

#include "dnasearch.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>

void compare(char *DNApat, char *DNAlib, int *ans, int *j, int *match){
	//static int j;
	for(int i=0; i<strlen(DNAlib);i++){
		for(int a=0; a<strlen(DNApat); a++){
			if(DNAlib[i+a]!=DNApat[a]){
				break;
			}
			if(a==strlen(DNApat)-1){
				//printf("%i\n",i);
				ans[*j]=i;
				//printf("%i\n",ans[j]);
				*j=*j+1;
				*match=1;
			}
		}
	}
}

void patCheck(char *DNApat, char *DNAlib){
	if(strlen(DNApat)> strlen(DNAlib)){
		puts("Sorry, entered bad pattern, too long, enter again");
		//scanf("%s", &DNApat[0]);
		return;
	}
	for(int i=0; i<strlen(DNApat);i++){
		if(DNApat[i]!= 'A' && DNApat[i]!='T' && DNApat[i]!='C' && DNApat[i]!='G'){
			if(DNApat[i]== 'a' || DNApat[i]=='t' || DNApat[i]=='c' || DNApat[i]=='g'){
				DNApat[i]=toupper(DNApat[i]);
			}
			else{
				puts("Sorry, entered bad pattern, please enter one with just A,T,C, or G\n");
				//scanf("%s", &DNApat[0]);
				break;
			}
		}
	}
}

int txtwriter(FILE *fp, char *DNAlib){
	int x=0;
	char ch;
	while((ch = fgetc(fp)) != EOF){
		if(x>14999){
			puts("Text is invalid, too long");
			return 1;
		}
		if(ch!=' ' && ch!= '\n'){
			if(ch!= 'A' && ch!='T' && ch!='C' && ch!='G'){
				if(ch== 'a' || ch=='t' || ch=='c' || ch=='g'){
						ch=toupper(ch);
				}
				else{
					puts("Text invalid, only put A,T,C, or G");
					return 1;
				}
			}
			DNAlib[x]=ch;
			x++;
		}
	}
	if(x==0){
		puts("Text is invalid, too short");
		return 1;
	}
	return 0;
}
