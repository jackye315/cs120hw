/*
Jack Ye
600.120
2/2/16
HW2
(917) 530-6467
jye24
jye24@jhu.edu
 */

#ifndef DNASEARCH_H_
#define DNASEARCH_H_
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include "dnasearch.h"

void compare(char *DNApat, char *DNAlib, int *ans, int *j, int *match);
void patCheck(char *DNApat, char *DNAlib);
int txtwriter(FILE *fp, char *DNAlib);


#endif /* DNASEARCH_H_ */
