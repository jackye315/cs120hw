/*
Jack Ye
600.120
2/2/16
HW2
(917) 530-6467
jye24
jye24@jhu.edu
 */

#include "dnasearch.h"

int main(){

	//test txtwritter
	FILE *fo=fopen("libTest.txt", "w");
	for(int i=0;i<0;i++){
		fprintf(fo, "a");
	}
	fclose(fo);
	FILE *fp=fopen("libTest.txt", "r");
	char testlib[15000];
	//test what happens if txt file too short
	txtwriter(fp, testlib);
	fclose(fp);

	FILE *fa=fopen("libTest.txt", "w");
	for(int i=0;i<15001;i++){
		fprintf(fa, "a");
	}
	fclose(fa);
	FILE *fb=fopen("libTest.txt", "r");
	char testlib1[15000];
	//test what happens if txt file too long
	txtwriter(fb, testlib1);
	fclose(fb);

	FILE *fc=fopen("libTest.txt", "w");
	for(int i=0;i<15001;i++){
		fprintf(fc, "b");
	}
	fclose(fc);
	FILE *fd=fopen("libTest.txt", "r");
	char testlib2[15000];
	//test what happens if other letters
	txtwriter(fd, testlib2);
	fclose(fd);

	FILE *fe=fopen("libTest.txt", "w");
	for(int i=0;i<15000;i++){
		fprintf(fe, "A");
	}
	fclose(fe);
	FILE *ff=fopen("libTest.txt", "r");
	char testlib3[15000];
	txtwriter(ff, testlib3);
	fclose(ff);
	char output=testlib3[30];
	assert(output=='A');
	output=testlib3[14999];
	assert(output=='A');
	output=testlib3[0];
	assert(output=='A');

	FILE *fg=fopen("libTest.txt", "w");
	for(int i=0;i<7500;i++){
		fprintf(fg, "A ");
	}
	fclose(fg);
	FILE *fh=fopen("libTest.txt", "r");
	char testlib4[15000];
	txtwriter(fh, testlib4);
	fclose(fh);
	output=testlib4[30];
	assert(output=='A');
	output=testlib4[7499];
	assert(output=='A');
	output=testlib4[0];
	assert(output=='A');
	output=testlib4[1];
	assert(output=='A');

	FILE *fi=fopen("libTest.txt", "w");
	for(int i=0;i<7500;i++){
		fprintf(fi, "A\n");
	}
	fclose(fi);
	FILE *fj=fopen("libTest.txt", "r");
	char testlib5[15000];
	txtwriter(fj, testlib5);
	fclose(fj);
	output=testlib5[30];
	assert(output=='A');
	output=testlib5[7499];
	assert(output=='A');
	output=testlib5[0];
	assert(output=='A');
	output=testlib5[1];
	assert(output=='A');

//test patCheck
	char test1[]="ATCGGGGGGGCCC";
	char test2[]="ATCCCCG";
	patCheck(test1, test2);

	char test3[]="ATTCB";
	char test4[]="ATCCCCG";
	patCheck(test3, test4);

	char test5[]="CAt";
	char test6[]="CATATCCCCGGGTTTCCAAGTCATGAT";
	patCheck(test5, test6);
	output=test5[2];
	assert(output=='T');


	//test compare
	int j=0;
	int ans[15000];
	int match =0;
	compare(test5, test6, ans, &j, &match);

	for(int i=0; i<j;i++){
		printf("%i ",ans[i]);
	}
	puts("\n");

	puts("All tests successful!");


	return 0;
}
